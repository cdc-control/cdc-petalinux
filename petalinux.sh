#!/bin/sh

cd $(dirname "$0")

# /opt should contain all additional software like ISE, Vivado etc.
docker run --rm -it --network host \
  -e CONTAINER=riviera \
  -e INIT_WD=$(pwd) \
  -e DISPLAY=$DISPLAY \
  -e HOME=$HOME \
  -v /etc/group:/etc/group:ro \
  -v /etc/passwd:/etc/passwd:ro \
  -v /etc/shadow:/etc/shadow:ro \
  -v /etc/sudoers.d:/etc/sudoers.d:ro \
  -v /etc/krb5.conf:/etc/krb5.conf:ro \
  -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
  -v $HOME:$HOME \
  --user $(id -u):$(id -g) \
  --cap-add=SYS_PTRACE \
  --security-opt seccomp=unconfined \
  --security-opt apparmor=unconfined \
  registry.cern.ch/ci4fpga/petalinux:2022.2 \
  /bin/bash
