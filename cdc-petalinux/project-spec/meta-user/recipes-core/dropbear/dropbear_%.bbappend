# The dropbear_%.bbappend looks like this:
# Dropbear: suppress gen_keys.
 
SRC_URI += "file://dropbear.initd;md5=d57baea52e22344ef6e78eaa73f1dad3"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
 
# Overwrite the dropbear configuration with my configuration.
do_install:append() {
	install -m 0755 ${WORKDIR}/dropbear.initd ${D}${sysconfdir}/init.d/dropbear
}



