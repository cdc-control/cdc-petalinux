SUMMARY = "Simple cdc-python application"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
PR = "r0"

inherit update-rc.d

# Add Python 3 dependencies
RDEPENDS:${PN} += "python3-core python3-logging python3-json python3-multiprocessing python3-datetime python3-mmap"

INITSCRIPT_NAME = "cdc-startup"
INITSCRIPT_PARAMS = "start 99 5 ."

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI = " \
    file://cdc-startup.sh \
    file://cdc-onewire-init \
"

do_install() {
    # Create required directories
    install -d ${D}${sysconfdir}/init.d
    install -d ${D}${bindir}

    # Install startup script 
    install -m 0755 ${WORKDIR}/cdc-startup.sh ${D}${sysconfdir}/init.d/cdc-startup

    # Install onewire script
    install -m 0755 ${WORKDIR}/cdc-onewire-init ${D}${bindir}/cdc-onewire-init
}

FILES:${PN} = " \
    ${sysconfdir}/init.d/cdc-startup \
    ${bindir}/cdc-onewire-init \
"