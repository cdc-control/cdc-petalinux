FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " file://bsp.cfg"
KERNEL_FEATURES:append = " bsp.cfg"
SRC_URI += "file://user_2023-05-08-10-20-00.cfg \
            file://user_2023-05-16-13-29-00.cfg \
            "


SRC_URI += "file://add-cern-devices-to-spidev.patch"
