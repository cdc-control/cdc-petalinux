# CDC Petalinux 
This repository contains the build files and build pipeline for the petalinux distribution for the CERN DCCT Calibrator controller.

To install this distribution, follow the instructions in the documentation, available at: https://current-calibrator.docs.cern.ch/
