BUILD_NAME=build
PROJECT_NAME=cdc-petalinux
PETALINUX_USER="petalinux"
CI_PETALINUX_PASSWORD="${PETALINUX_PASSWORD:-zynqdev}"

# create a "build" project
petalinux-create -t project -n $BUILD_NAME --template zynq

# run the hw config
petalinux-config --silentconfig --get-hw-description=design_1_wrapper.xsa -p $BUILD_NAME

# run the config
petalinux-config --silentconfig --project $BUILD_NAME/

# set the user name and password from the secrets
CONFIG_FILE="$PROJECT_NAME/project-spec/configs/rootfs_config"
sed -i "s/CONFIG_ADD_EXTRA_USERS=\"petalinux:zynqdev;\"/CONFIG_ADD_EXTRA_USERS=\"$PETALINUX_USER:$CI_PETALINUX_PASSWORD;\"/g" $CONFIG_FILE

# copy over previos config
rsync -av --exclude='hw-description' $PROJECT_NAME/project-spec $BUILD_NAME/



# build
petalinux-build -p $BUILD_NAME && \
# package
petalinux-package --boot --force --fsbl $BUILD_NAME/images/linux/zynq_fsbl.elf --fpga $BUILD_NAME/project-spec/hw-description/design_1_wrapper.bit  --u-boot --project $BUILD_NAME && \
# do bsp
petalinux-package --bsp -p $BUILD_NAME -o cdc-petalinux.bsp

