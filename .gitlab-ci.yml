variables:
  BUILD_NAME: build
  GIT_SUBMODULE_STRATEGY: recursive
  PROJECT_NAME: cdc-petalinux

default:
  image: gitlab-registry.cern.ch/cce/docker_build/petalinux:2022.2
  tags:
    - docker-privileged-xl

stages:
  - build
  - package

setup-job:
  stage: build
  # execute this only when there are changes in the following paths
  # only:
  #  changes:
  #     - cdc-petalinux/*
  #     - design_1_wrapper.bit
  #     - design_1_wrapper.xsa
  script: ./setup-petalinux-cdc.sh
  artifacts:
    when: always
    paths:
      - $BUILD_NAME/build/tmp/work/zynq_generic-xilinx-linux-gnueabi/petalinux-image-minimal/1.0-r0/temp/log.*
      - $BUILD_NAME/build/tmp/work/cortexa9t2hf-neon-xilinx-linux-gnueabi/cdc-python/**/log.*
      - $BUILD_NAME/build/build.log
      - $BUILD_NAME/images/linux/image.ub
      - $BUILD_NAME/images/linux/BOOT.BIN
      - $BUILD_NAME/images/linux/boot.scr
      - cdc-petalinux.bsp
      

create-sd-image:
  stage: package
  script:
    # 1. Set up the environment for creating SD card images
    - apt-get update && apt-get install -y parted losetup kpartx git

    # 2. Create an empty SD card image (let's say 2GB)
    - dd if=/dev/zero of=sdcard.img bs=1M count=2000

    # Create the partitions on the SD card image
    - parted sdcard.img --script mklabel msdos
    - parted sdcard.img --script mkpart primary fat32 1M 500M
    - parted sdcard.img --script mkpart primary fat32 500M 100%

    # check the result 
    - fdisk -l sdcard.img

    # create loop device
    - sudo mknod -m660 /dev/loop10 b 7 10
    - sudo chown root:disk /dev/loop10

    # Generate a MAC address and store it in an environment variable
    - export TARGET_MAC_ADDRESS="00:18:3e:04:71:c7"

    # Create the uEnv.txt file with the generated MAC address
    - echo "ethaddr=${TARGET_MAC_ADDRESS}" > uEnv.txt
    
    # Report the chosen MAC address to the job's output
    - echo "Chosen MAC address ${TARGET_MAC_ADDRESS}"

    # Setup loop devices for the partitions
    - sudo losetup -P /dev/loop10 sdcard.img
    - LOOP_DEV="/dev/loop10"
    - sudo kpartx -avs $LOOP_DEV
    - sudo ls /dev/mapper/
    - sudo partprobe $LOOP_DEV
    - ls /dev/loop*

    # 3. Mount each partition and populate them with the necessary files
    # For the 1st partition
    - sudo mkdir -p /mnt/part1
    - sudo mkfs.vfat /dev/mapper/loop10p1
    - sudo fatlabel /dev/mapper/loop10p1 BOOT
    - sudo mount /dev/mapper/loop10p1 /mnt/part1
    - sudo cp $BUILD_NAME/images/linux/* /mnt/part1/
    - sudo cp uEnv.txt /mnt/part1/
    - sudo umount /mnt/part1

    # For the 2nd partition
    - sudo mkdir -p /mnt/part2
    - sudo mkfs.vfat /dev/mapper/loop10p2
    - sudo fatlabel /dev/mapper/loop10p2 APP
    - sudo mount /dev/mapper/loop10p2 /mnt/part2
    - sudo cp -r cdc-control-application /mnt/part2
    - sudo umount /mnt/part2

    # 4. Unmount the partitions and detach loop device
    - sudo kpartx -dvs $LOOP_DEV
    - sudo losetup -d $LOOP_DEV

  artifacts:
    paths:
      - sdcard.img
